#!/usr/bin/env ruby

require 'gtk3'
require 'rb-inotify'
require 'glib2'
require 'open3'
require 'fileutils'

class AudioApp < Gtk::Window

  attr_reader :threads
  attr_reader :notifier

  def setup(filename)
    set_title "Normalize Audio %s" % [ filename ]
    puts "Working on %s" % [ filename ]
    image_filename = filename.sub(/flac$/, 'png')
    @filename = filename
    @good_filename =  './good/' + filename
    @bad_filename =  './bad/' + filename
    @orig_filename = @origdir + '/' + filename 
    @work_filename = @workdir + '/' + filename 

    puts "%s %s" % [ @orig_img1,  @orig_img2 ]
    if @watch_1
      @watch_1.close
      watch_1 = nil
    end
    if @watch_2
      @watch_2.close
      watch_2 = nil
    end

    @watch_1 = @notifier.watch(@orig_img1, :close_write) { |event|
      puts "1 %s was mod! '%s'" % [  @orig_img1, event.flags ]
      begin
        bardejov = GdkPixbuf::Pixbuf.new(:file => @orig_img1 )
        @image_1.pixbuf = bardejov
      rescue => x
        puts "XXXXX %s" % [ x ]
      end
    }

    @watch_2 = @notifier.watch(@orig_img2, :close_write) { |event|
      puts "2 %s was mod! '%s'" % [  @orig_img2, event.flags ]
      begin
        bardejov = GdkPixbuf::Pixbuf.new(:file => @orig_img2 )
        @image_2.pixbuf = bardejov
      rescue => x
        puts "YXXXXX %s" % [ x ]
      end
    }

    cmds1 = [ 'ffmpeg', '-y', '-i', @orig_filename, '-f', 'lavfi', '-i', 'color=c=black:s=640x320', '-filter_complex',
         '[0:a]showwavespic=s=640x320:colors=white[fg];[1:v][fg]overlay=format=auto',
         '-frames:v', '1', @orig_img1 ]
    cmds2 = [ 'ffmpeg', '-y', '-i', @work_filename, '-f', 'lavfi', '-i', 'color=c=black:s=640x320', '-filter_complex',
         '[0:a]showwavespic=s=640x320:colors=white[fg];[1:v][fg]overlay=format=auto',
         '-frames:v', '1', @orig_img2 ]
    Open3.popen3( * cmds1 ) { |stdin, stdout, stderr, wait_thr|
      pid = wait_thr.pid # pid of the started process.
      exit_status = wait_thr.value # Process::Status object returned.
      puts "%s %s" %  [ pid, exit_status ]
    }
    Open3.popen3( * cmds2 ) { |stdin, stdout, stderr, wait_thr|
      pid = wait_thr.pid # pid of the started process.
      exit_status = wait_thr.value # Process::Status object returned.
      puts "%s %s" %  [ pid, exit_status ]
    }

    @watch_3 = @notifier.watch(@work_filename, :close_write) { |event|
      puts "3 %s was mod! '%s'" % [  @work_filename, event.flags ]
      begin
        Open3.popen3( * cmds2 ) { |stdin, stdout, stderr, wait_thr|
          pid = wait_thr.pid # pid of the started process.
          exit_status = wait_thr.value # Process::Status object returned.
          puts "%s %s" %  [ pid, exit_status ]
        }
      rescue => x
        puts "YXXXXX %s" % [ x ]
      end
    }
  end

  def initialize(mainloop)
    super()
    @notifier = INotify::Notifier.new

    @workdir = './work'
    @origdir = './mono'

    @files = Array.new

    Dir.entries(@workdir + '/.').keep_if { |x| x.match? /\.flac$/ }.each do |file|
       next if File.exist? "./good/%s" % [ file ]
       next if File.exist? "./bad/%s" % [ file ]
       @files.push file
    end

    set_title "Normalize Audio"
    
    signal_connect "destroy" do |x|
      @notifier.stop
      @notifier.close
      threads.each { |thr| puts thr; Thread.kill( thr ) }
    end

    set_default_size 500, 500
    set_window_position Gtk::WindowPosition::CENTER
    hboxB = Gtk::Box.new(:horizontal, 6)

    @orig_image_file = 'images/Nl-aan.png'
    @orig_img1 = './work_dir/image_1.png'
    @orig_img2 = './work_dir/image_2.png'
    FileUtils.mkdir_p 'work_dir'

    FileUtils.touch(@orig_img1)
    FileUtils.touch(@orig_img2)

    bardejov = GdkPixbuf::Pixbuf.new(:file => @orig_img1)
    @image_1 = Gtk::Image.new(pixbuf: bardejov)
    bardejov = GdkPixbuf::Pixbuf.new(:file => @orig_img2)
    @image_2 = Gtk::Image.new(pixbuf: bardejov)

    setup(@files[0])

    begin
      button = Gtk::Button.new(label: "Good")
      hboxB.pack_start(button)

      button.signal_connect('clicked') {
        @files.shift
        File.link @work_filename, @good_filename
        file = @files[0];
        setup(file);
      }
    end

    begin
      button = Gtk::Button.new(label: "Bad")
      hboxB.pack_start(button)

      button.signal_connect('clicked') {
        File.link @work_filename, @bad_filename

        @files.shift
        file = @files[0];
#        audio = file.sub(/wav/, 'png');
#        file = './images/' + audio;
#        bardejov = GdkPixbuf::Pixbuf.new(:file => file)
#        @image_1.pixbuf =bardejov
      }
    end
 
    begin
      button = Gtk::Button.new(label: "Play")
      hboxB.pack_start(button)

      button.signal_connect('clicked') {
        audio = @files[0]
        puts audio
        file =  @work_filename;
        puts file
        @pid = fork { exec 'mplayer', file }
        puts file
      }
    end

    vboxB =  Gtk::Box.new(:vertical, 4)
    begin
      begin
        vboxB.pack_start( b = Gtk::Button.new(label: "Revert"))
        b.signal_connect('clicked') {
          audio = @files[0]
          FileUtils.cp(@orig_filename, @work_filename);
          puts "reverted"
        }
      end
      begin
        vboxB.pack_start( b = Gtk::Button.new(label: "Noise"))
        b.signal_connect('clicked') {
          audio = @files[0]
          puts audio
          file =  @work_filename;
          puts file
          @pid = fork { exec 'filters/remove_noise', @work_filename, @orig_img2 }
          puts file
        }
      end
      begin
        vboxB.pack_start( b = Gtk::Button.new(label: "Normal"))
        b.signal_connect('clicked') {
          audio = @files[0]
          puts audio
          file =  @work_filename;
          puts file
          @pid = fork { exec 'filters/normalize', @work_filename, @orig_img2 }
          puts file
        }
      end
      begin
        vboxB.pack_start( b = Gtk::Button.new(label: "Trim"))
        b.signal_connect('clicked') {
          audio = @files[0]
          puts audio
          file =  @work_filename;
          puts file
          @pid = fork { exec 'filters/remove_silence', @work_filename, @orig_img2 }
          puts file
        }
      end
      begin
        vboxB.pack_start( b = Gtk::Button.new(label: "Declick"))
        b.signal_connect('clicked') {
          audio = @files[0]
          puts audio
          file =  @work_filename;
          puts 'exec: ' + [ 'filters/remove_silence', @work_filename, @orig_img2 ].join(' ')
          @pid = fork { exec 'filters/remove_click', @work_filename, @orig_img2 }
          puts file
        }
      end
    end

    vboxM = Gtk::Box.new(:vertical, 2)
    vbox = Gtk::Box.new(:vertical, 2)
    vboxM.add(hboxB)

    event_box_1 = Gtk::EventBox.new.add(@image_1)
    event_box_1.signal_connect("button_press_event") do
      puts "Clicked 1."
    end
    event_box_2 = Gtk::EventBox.new.add(@image_2)
    event_box_2.signal_connect("button_press_event") do
      puts "Clicked 2."
    end
    vbox.add(event_box_1);
    vbox.add(event_box_2);

    hboxM = Gtk::Box.new(:horizontal, 2)

    hboxM.add(vbox)
    hboxM.add(vboxB)

    vboxM.add(hboxM)

    add(vboxM);

    @threads = []
    @threads << Thread.new { puts "Start notifier"; @notifier.run; puts "stop notifier" }

  end
end

mainloop = GLib::MainLoop.new
window = AudioApp.new(mainloop)

window.signal_connect "destroy" do
  GLib::Timeout.add(100) do
    mainloop.quit
    false
  end
end

GLib::Timeout.add(1) do
  window.show_all
  false
end

puts 'mainloop'
mainloop.run
puts 'exiting'

